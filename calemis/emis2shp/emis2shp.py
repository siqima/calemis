import numpy as np
from netCDF4 import Dataset
import netCDF4 as nc
import shapefile
import dbf
import os

env_dist  = os.environ

emis_in = "emis_mole_mobile_2017ann_US01_cmaq_cb6ae7_2017gb_17j.ncf"


shpf = "polygon_1km.shp"
shpo = "US01_emis_mobile.shp"


input = shapefile.Reader(shpf)
shapes = input.shapes() # -> the geometries in a list
fields = input.fields[1:] #-> the fields definition in a list
fields_name = [field[0] for field in fields] #-> the fields names in a list
attributes = input.records() #-> the attributes in a list

print(fields)
print(fields_name)
print(len(attributes))
print(attributes[0][:])

# Create a new shapefile in memory
outfile = shapefile.Writer(shpo) 
outfile.fields = input.fields #list(input.fields)

# Open and read emission file
emis_fid = Dataset(emis_in, 'r')
voc_in = emis_fid.variables['VOCANN'][0,0,:,:]
nox_in = emis_fid.variables['NOXANN'][0,0,:,:]
co_in = emis_fid.variables['COANN'][0,0,:,:]
so2_in = emis_fid.variables['SO2ANN'][0,0,:,:]
nh3_in = emis_fid.variables['NH3ANN'][0,0,:,:]
pm_in = emis_fid.variables['PM25ANN'][0,0,:,:]
pec_in = emis_fid.variables['PECANN'][0,0,:,:]
poc_in = emis_fid.variables['POCANN'][0,0,:,:]
pmc_in = emis_fid.variables['PMCANN'][0,0,:,:]


ny = nox_in.shape[0]
nx = nox_in.shape[1]

nn = 0

for rec in input.iterShapeRecords():
  nn = nn+1
  #rec.append(i)

### fishnet1
  '''
  ii = int(nn%nx)
  if (ii==0):
   ii = nx-1
  else:

  jj = int((nn-1)/nx)

  print(nn-1,jj,ii)
  '''
### fishnet2
  ii = int(rec.record[0])-1
  jj = int(rec.record[1])-1

  emis_shp = voc_in[jj,ii]
  rec.record[2]=emis_shp

  emis_shp = nox_in[jj,ii]
  rec.record[3]=emis_shp

  emis_shp = so2_in[jj,ii]
  rec.record[4]=emis_shp
  emis_shp = co_in[jj,ii]
  rec.record[6]=emis_shp
  emis_shp = nh3_in[jj,ii]
  rec.record[5]=emis_shp
  emis_shp = pm_in[jj,ii]
  rec.record[7]=emis_shp
  emis_shp = pec_in[jj,ii]
  rec.record[8]=emis_shp
  emis_shp = poc_in[jj,ii]
  rec.record[9]=emis_shp
  emis_shp = pmc_in[jj,ii]
  rec.record[10]=emis_shp

 # Add the modified record to the new shapefile 
  outfile.record(*rec.record)
  outfile.shape(rec.shape)


  



