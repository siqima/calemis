#! /bin/bash -f
# Merge the sector emission into one

export SMOKE_LOCATION=  # SMOKE location

domain=US01 
export BASEDIR=  # directory of this file
export FILELIST=$BASEDIR/Filelist.sector.lst
export EMIS_DIR= # directory of input emission files

export OUTDIR= # directory of outputs

if [[ ! -d $OUTDIR ]]; then
  mkdir -p $OUTDIR
fi

export GRID_NAME=$domain


for line in $(cat ${BASEDIR}/mrg_day.csv)
do


repd=$(echo $line | tr "," "\n")
echo "repd=" $repd

for mmdd in $repd 
do
  
  rundate=2017${mmdd:0:2}
  echo "Processing $rundate" 

  export AFDUST=$EMIS_DIR/afdust/emis_mole_afdust_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export AG=$EMIS_DIR/ag/emis_mole_ag_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export AIRPORTS=$EMIS_DIR/airports/emis_mole_airports_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export NONPT=$EMIS_DIR/nonpt/emis_mole_nonpt_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export NONROAD=$EMIS_DIR/nonroad/emis_mole_nonroad_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export NP_OILGAS=$EMIS_DIR/np_oilgas/emis_mole_np_oilgas_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export ONROAD=$EMIS_DIR/onroad/emis_mole_onroad_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export RAIL=$EMIS_DIR/rail/emis_mole_rail_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export RWC=$EMIS_DIR/rwc/emis_mole_rwc_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf

  export OUTFILE=${OUTDIR}/emis_mole_all_${rundate}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export LOGFILE=$BASEDIR/LOGS/mrggrid_${domain}_${rundate}.log

  export PROMPTFLAG=N
  rm -f $LOGFILE
  rm -f $OUTFILE

#  echo $LOGFILE

  ${SMOKE_LOCATION}/mrggrid

  if [[ ! -e $OUTFILE ]]; then
    echo $OUTFILE not generated!
    exit
  fi

done # mmdd

done #< ${BASEDIR}/../rep_day.csv
