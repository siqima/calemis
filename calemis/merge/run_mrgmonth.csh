#! /bin/bash -f
# Merge the monthly emission into one

export SMOKE_LOCATION=   # SMOKE location

domain=US01 
export BASEYEAR=2017
export sector=onroad #afdust ag airports  nonpt  nonroad  np_oilgas  onroad_ff10  rail  rwc all

export BASEDIR=  # directory of this file
export FILELIST=$BASEDIR/Filelist.month.lst
export EMIS_DIR= # directory of input emission files

export OUTDIR=  # directory of outputs


if [[ ! -d $OUTDIR ]]; then
  mkdir -p $OUTDIR
fi


  export M1=$EMIS_DIR/emis_mole_${sector}_201701avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M2=$EMIS_DIR/emis_mole_${sector}_201702avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M3=$EMIS_DIR/emis_mole_${sector}_201703avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M4=$EMIS_DIR/emis_mole_${sector}_201704avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M5=$EMIS_DIR/emis_mole_${sector}_201705avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M6=$EMIS_DIR/emis_mole_${sector}_201706avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M7=$EMIS_DIR/emis_mole_${sector}_201707avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M8=$EMIS_DIR/emis_mole_${sector}_201708avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M9=$EMIS_DIR/emis_mole_${sector}_201709avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M10=$EMIS_DIR/emis_mole_${sector}_201710avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M11=$EMIS_DIR/emis_mole_${sector}_201711avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  export M12=$EMIS_DIR/emis_mole_${sector}_201712avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf

  export OUTFILE=${OUTDIR}/emis_mole_${sector}_${BASEYEAR}avg_${domain}_cmaq_cb6ae7_2017gb_17j.ncf
  #export LOGFILE=$BASEDIR/LOGS/mrggrid_${sector}_${domain}_${BASEYEAR}.log

  export PROMPTFLAG=N
  export MRG_DIFF_DAYS=TRUE
  export G_STDATE=2017001
  export G_STTIME=0
  export G_TSTEP=10000
  export G_RUNLEN=250000
  rm -f $LOGFILE
  rm -f $OUTFILE

#  echo $LOGFILE

  ${SMOKE_LOCATION}/mrggrid

  if [[ ! -e $OUTFILE ]]; then
    echo $OUTFILE not generated!
    exit
  fi
