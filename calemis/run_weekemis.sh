#!/bin/bash -f
# This script will read the hourly emissions, calculate monthly emissions for each sector.

ioapi_dir=   # location of I/O API library


# start and end dates (YYYYMMDD)
srdate=20170101 
erdate=20170107 

work_dir=    # directory of this script
indir=       # directory of input files
outdir=      # directory of output files

base_year=2017
mech=cmaq_cb6ae7
ver=2017gb_17j

export IF_MRG=Y  # calculate monthly average and then get monthly total emission(Y), or calculate monthly emission directly(N). Use Y for the first time run.

for domain in US01 
do
  
  for sector in afdust ag airports  nonpt  nonroad  np_oilgas  onroad_ff10  rail  rwc
  do
    if [[ $sector == "rail" || $sector == "np_oilgas" ]]; then
      export group=2
    else
      export group=1
    fi
    echo "group=$group"

    if [[ $group -eq 1 ]]; then

      while IFS=, read -r repd1 repd2 repd3 repd4
      do
        echo "$repd1|$repd2|$repd3|$repd4"

        date1=${base_year}${repd1:0:2}
        export CALDATEJ=`date -d "${date1}01" +%Y%j`
        export INFILE1=${indir}/${sector}/emis_mole_${sector}_${base_year}${repd1}_${domain}_${mech}_${ver}.ncf
        export INFILE2=${indir}/${sector}/emis_mole_${sector}_${base_year}${repd2}_${domain}_${mech}_${ver}.ncf
        export INFILE3=${indir}/${sector}/emis_mole_${sector}_${base_year}${repd3}_${domain}_${mech}_${ver}.ncf
        export INFILE4=${indir}/${sector}/emis_mole_${sector}_${base_year}${repd4}_${domain}_${mech}_${ver}.ncf

        if [[ $sector == "onroad" ]]; then
          export INFILE1=${indir}/${sector}/emis_mole_${sector}_ff10_${base_year}${repd1}_${domain}_${mech}_${ver}.ncf
          export INFILE2=${indir}/${sector}/emis_mole_${sector}_ff10_${base_year}${repd2}_${domain}_${mech}_${ver}.ncf
          export INFILE3=${indir}/${sector}/emis_mole_${sector}_ff10_${base_year}${repd3}_${domain}_${mech}_${ver}.ncf
          export INFILE4=${indir}/${sector}/emis_mole_${sector}_ff10_${base_year}${repd4}_${domain}_${mech}_${ver}.ncf
        fi

        if [[ $sector == "afdust" ]]; then
          export INFILE1=${indir}/${sector}/emis_mole_${sector}_${base_year}${repd1}_${domain}_${mech}_${ver}_xportfrac.ncf
          export INFILE2=${indir}/${sector}/emis_mole_${sector}_${base_year}${repd2}_${domain}_${mech}_${ver}_xportfrac.ncf
          export INFILE3=${indir}/${sector}/emis_mole_${sector}_${base_year}${repd3}_${domain}_${mech}_${ver}_xportfrac.ncf
          export INFILE4=${indir}/${sector}/emis_mole_${sector}_${base_year}${repd4}_${domain}_${mech}_${ver}_xportfrac.ncf
        fi

        export OUTFILE=${outdir}/${sector}/emis_mole_${sector}_${date1}avg_${domain}_${mech}_${ver}.ncf

        #export LOGFILE=log.weekemis
        #rm $LOGFILE 
        if [[ -e $OUTFILE ]];then
          rm $OUTFILE 
        fi
 
        cd calavg
        ./CAL_WEEKEMIS.exe

        if [[ ! -e $OUTFILE ]];then
          echo $OUTFILE not generated!
          exit
        fi

        cd ..

      done < ${work_dir}/rep_day.csv
    else  # group
      while IFS=, read -r repd
      do
        echo "|$repd|"
        date1=${base_year}${repd:0:2}
        export CALDATEJ=`date -d "${date1}01" +%Y%j`
        export INFILE1=${indir}/${sector}/emis_mole_${sector}_${base_year}${repd}_${domain}_${mech}_${ver}.ncf

        export OUTFILE=${outdir}/${sector}/emis_mole_${sector}_${date1}avg_${domain}_${mech}_${ver}.ncf

        if [[ -e $OUTFILE ]];then
          rm $OUTFILE
        fi

        cd calavg
        ./CAL_WEEKEMIS.exe

        if [[ ! -e $OUTFILE ]];then
          echo $OUTFILE not generated!
          exit
        fi

        cd ..

      done < ${work_dir}/rep_1day.csv

    fi # group
  done # sector

done # domain


